# Mobile wave ontology #
An ontology modeling smartphone extending Semantic Sensor Network Ontology 

## Static device capabilities ##
![mobile-wave-device-owl.png](https://bitbucket.org/repo/b8ryEy/images/3783387121-mobile-wave-device-owl.png)

## Observation ##
![mobile-wave-ontology.png](https://bitbucket.org/repo/b8ryEy/images/75646157-mobile-wave-ontology.png)